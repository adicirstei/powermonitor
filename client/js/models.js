var app = app || {};

(function () {
  
  var Settings = Backbone.Model.extend({
    localStorage: new Backbone.LocalStorage('powermonitor-settings'),
    defaults:{
      emails: [],
      from: 'telefonul de acasa'
    },
    
    deleteEmail: function(email) {
      var list = this.get('emails');
      var idx;
      var todel = list.find(function(e,i) {
        if (e && (e.email === email)) idx = i;
        return e && e.email === email;
      });

      delete list[idx];
      list = _.compact(list);
      
      this.set('emails', list);
      
      this.save();

    },
    flipEnabled: function(email, enabled) {
      var list = this.get('emails');
      var el = list.find(function(e,i) {
        if (e && (e.email === email)) idx = i;
        return e && e.email === email;
      });
      el.enabled = enabled;
      this.set('emails', list);
      this.save();
    
    }
    
  });
  
  app.settings = new Settings({id: 'powermon'});
  app.settings.fetch();

})();