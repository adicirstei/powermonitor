var app = app || {};


(function() {
  // We'll ask the browser to use strict code to help us catch errors earlier.
  // https://developer.mozilla.org/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode
  'use strict';
  var lastStatus = 'unknown';
  
  var batt = navigator.battery || navigator.mozBattery || navigator.webkitBattery; 
  
  batt.addEventListener("chargingchange", updateBatteryStatus); 
  
  batt.addEventListener("levelchange", updateBatteryStatus);

  app.view = new app.AppView({model: app.settings});

  updateBatteryStatus();
  function start() {
/*
    var message = document.getElementById('battery');

    message.textContent = translate('message');
    
    message.setAttribute("value", batt.level);
    
    if (lastStatus === "charging"){
      message.classList.add('battery-charging');
    } else {
      message.classList.remove('battery-charging');
    }
*/
    
    app.view.render();

  }

  function sendMessage(msgObj) {
    var request = new XMLHttpRequest({mozSystem: true});
    request.open('POST', 'http://ac-webapis.appspot.com/rmail/', true);
    
    request.addEventListener('load', function(event) {
        console.log(request.response);
    });            
    try {
        request.send(JSON.stringify(msgObj));
    } catch (err) {
        console.error(err);
    }
  }
  
  
  function updateBatteryStatus() {
    
    var prevStatus = lastStatus;
    var dest = _.compact(app.settings.get('emails').map(function(e) {return (e.enabled? e.email : null); }))
    var to = dest.join(';');
    lastStatus = (batt.charging || batt.level == 1? "charging" : "discharging");
    
    app.settings.set('level', batt.level);
    app.settings.set('status', lastStatus);
    
    start();
    
    if (prevStatus !== lastStatus && prevStatus !== 'unknown' && dest.length) {
      sendMessage({
        sender:app.settings.get('from') + " <adicirstei@gmail.com>", 
        to: to, 
        subject: (lastStatus === "charging"? 'A venit curentul': 'S-a oprit curentul'), 
        body:(lastStatus === "charging"? 'A venit curentul': 'S-a oprit curentul') 
      });
    }
  }
  
  app.updateBatteryStatus = updateBatteryStatus;

})();
