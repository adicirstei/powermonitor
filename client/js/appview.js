var app = app || {};

(function() {
  
  var ItemView = Backbone.View.extend({
    events: {
      'click .delete-button': 'deleteItem',
      'click input': 'toggleEnabled'
    },
    tagName: 'li',
    deleteItem: function() {
      var e = this.model.get('email');
      
      app.settings.deleteEmail(e);
      this.remove();
    },
    
    toggleEnabled: function() {
      var email = this.model.get('email');
      var ch = this.input.is(':checked');

      app.settings.flipEnabled(email, ch);
    },
    
    template: function(obj) {
      return '<aside><label class="pack-switch"><input type="checkbox" ' + (obj.enabled? 'checked':'') + '><span></span></label></aside><aside class="pack-end gaia-icon  icon-closecancel delete-button"></aside><p>' + obj.email + '</p>';
    },
    
    render: function() {
      
      this.$el.html(this.template(this.model.toJSON()));
      this.input = $('input', this.$el);
      return this;
    }
  
  });
  
  
  app.AppView = Backbone.View.extend({
    el: '#app-view',
    events: {
      'keyup #from-input': 'fromChange',
      'click #add-email': 'addEmail',
      'click #refresh-btn': 'refreshStatus'
    },
    
    initialize: function() {
      this.listenTo(this.model, 'change:status', this.onStatusChange);
    },
    
    refreshStatus: function() {
      app.updateBatteryStatus();
    },
    
    
    onStatusChange: function() {
      if (this.model.get('status') === "charging"){
        $('#battery').addClass('battery-charging');
      } else {
        $('#battery').removeClass('battery-charging');
      }
    },
    
    fromChange: function () {
      this.model.set('from', $('#from-input').val());
      this.model.save();
    },
    
    addEmail: function(){
      
      var e = $('#add-input').val().trim();
      if (!e) return;
      var elist = app.settings.get('emails');
      
      if(!elist.find(function(em) {return em.email === e;})) {
        var newel = {email: e, enabled:true};
        elist.push(newel);
        app.settings.set('emails', elist);
        app.settings.save();
        this.render();
        
        //this.$elist.append(new ItemView({model: new Backbone.Model(newel)}).render().el);
      }
      
    },
    
    template: function(obj) {
      var ret = '<header><menu role="toolbar"><button class="gaia-icon icon-reload" id="refresh-btn">.</button></menu>  <h1>Power Monitor</h1></header>\n  <article><p>This app is monitoring battery charging status and notify on change.</p>\n<p  class="battery-level">' + obj.status + ' ' + (obj.level * 100) + '%</p><progress id="battery" value="'
      + obj.level +
      '" class="' + (obj.status === 'charging'? 'battery-charging': '') + '" ></progress><form role="search"><p><input type="text" id="from-input" placeholder="from" value="'
          
          + obj.from + '"><button type="reset">Clear</button></p></form><ul data-type="list" id="e-list">'
          
        
      ret += '</ul><form role="search" class="bottom"><button type="submit" id="add-email">Add</button><p><input id="add-input" type="email" placeholder="add new email"><button type="reset"></button></p></form></article>'
      return ret;
    },
    
    render: function() {
      
      this.$el.html(this.template(this.model.toJSON()));
      var l = this.$elist = $('#e-list');
      this.model.get('emails').forEach(function (e) {
        if (!e) return;
        l.append(new ItemView({model: new Backbone.Model(e)}).render().el);
      });
      
      return this;
    
    }
  
  });
})();